package com.m.wechathelperforgroup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by m on 2018/7/17.
 */

public class ItemAdapter extends ArrayAdapter {
    private final int resourceId;

    public ItemAdapter(Context context, int textViewResourceId, List<Linkman> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Linkman linkman = (Linkman) getItem(position); // 获取当前项的Fruit实例
        View view = LayoutInflater.from(getContext()).inflate(resourceId, null);//实例化一个对象
        TextView wechatid = (TextView) view.findViewById(R.id.item_wechatid);//获取该布局内的图片视图
        TextView name = (TextView) view.findViewById(R.id.item_name);//获取该布局内的文本视图
        wechatid.setText(linkman.getWechatId());//为图片视图设置图片资源
        name.setText(linkman.getName());//为文本视图设置文本内容
        return view;
    }

}
